Usage ctypes
=====

.. _instruction:

Instruction
------------

To use an example of ctype create and complile function on C and create the same function on python:

.. code-block:: console

   gcc -shared -Wl,-soname,c_fibonacci.so -o c_fibonacci.so -fPIC c_fibonacci.c

Running
----------------

you can run the ``pyfib()`` function with timing:

.. autofunction:: print(timeit.timeit('pyfib(32)', 'from __main__ import pyfib', number=1))

In some cases :py:func:`pyfib`
will raise an exception.

.. autoexception:: pyfib.ZeroDivision

For example:

>>> import pyfib
>>> pyfib(0)
1245978