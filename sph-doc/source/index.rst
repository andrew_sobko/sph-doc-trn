.. sph-doc documentation master file, created by
   sphinx-quickstart on Tue Dec  7 16:27:00 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sph-doc's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   doc-1
   doc-2
   doc-3



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
